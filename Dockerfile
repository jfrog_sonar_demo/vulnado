FROM maven:3.8.4-jdk-11-slim AS build
WORKDIR /app
COPY pom.xml .
COPY src src
RUN mvn package -DskipTests

FROM openjdk:11.0.13-jre-slim
WORKDIR /app
COPY --from=build /app/target/*.jar app.jar
EXPOSE 8081
ENTRYPOINT ["java", "-jar", "app.jar"]